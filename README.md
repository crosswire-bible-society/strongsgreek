# strongsgreek

Strong's Greek Dictionary of Bible Words

To build the strongsgreek dic execute:
cd script
make
cd ../
mkdir tei
cp script/strongsgreek.tei.xml tei/

And for test:
tei2mod /home/cyrille/.sword/modules/lexdict/zld/strongsgreek/ strongsgreek.tei.xml -z
